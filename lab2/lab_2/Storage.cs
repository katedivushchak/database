﻿using System;
using System.Collections.Generic;
using Npgsql;
using System.Data;

namespace lab_2
{
    public class Storage
    {
        private string host;
        private int port;
        private string user;
        private string password;
        private string database;

        private string wholeParam;

        public Storage(string _host, int _port, string _user, string _password, string _database)
        {
            this.host = _host;
            this.port = _port;
            this.user = _user;
            this.password = _password;
            this.database = _database;

            wholeParam = "Server=" + host + ";Port=" + port.ToString() + ";User Id=" + user + ";Password=" + password + ";Database="+database+";";
        }

        void Analyze(string query)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                con.Open(); //opens the connection

                List<string> analyzeList = new List<string>();
                
                using (NpgsqlCommand command = new NpgsqlCommand("EXPLAIN ANALYSE " + query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        analyzeList.Add(reader[0].ToString());
                    }
                }

                 
                Console.WriteLine(analyzeList[analyzeList.Count-2]);
                Console.WriteLine(analyzeList[analyzeList.Count - 1]);
                 

                con.Close();
            }
        }

        public bool InputStore(string name, string owner)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "INSERT INTO public.store(name, owner)" +
                                    "VALUES('" + name + "', '" + owner + "'); ";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool UpdateStore(int id, string name, string owner)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "UPDATE public.store SET name='"+name+"', owner='"+owner + "' WHERE route_id ="+id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool InputProduct(string name, int price)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "INSERT INTO public.product(name, price)" +
                                    "VALUES('" + name + "', " + price +"); ";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                    catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool UpdateProduct(int id, string name, int price)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "UPDATE public.product SET name='"+ name + "', price="+price +" WHERE product_id="+id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool InputEmployee(string name, string surname, int work_experience, int store_id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "INSERT INTO public.employee (name, surname, work_experience, work_store_id) " +
                                    "VALUES('"+name+ "', '" + surname+ "', " + work_experience + ", "+ store_id + "); ";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Wrong data.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool UpdateEmployee(int id, string name, string surname, int work_experience, int store_id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "UPDATE public.employee SET name='" + name + "', surname='" + surname +
                                    "', work_experience=" + work_experience + ", work_store_id=" + store_id + " WHERE employee_id = " + id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Wrong data.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        /*public List<Product> GetAllStoresWithProducts()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                List<Product> tempList = new List<Product>();

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                string query = "SELECT public.store.id_store, public.store.name, owner, public.product.id_product, public.product.name, price " +
                                    "FROM public.R1 " +
                                        "INNER JOIN public.store ON public.R1.id_store = public.store.id_store " +
                                        "INNER JOIN public.product ON public.R1.id_product = public.product.id_product";

                Analyze(query);

                using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        bool isNew = true;
                        foreach(Store soloStore in tempList)
                        {
                            if(soloStore.id == int.Parse(reader[0].ToString()))
                            {
                                isNew = false;

                                Product tempProduct;
                                tempProduct.id = int.Parse(reader[3].ToString());
                                tempProduct.name = reader[4].ToString();
                                tempProduct.price = int.Parse(reader[5].ToString());
                                soloStore.products.Add(tempProduct);
                            }
                        }

                        if (isNew)
                        {
                            Store tempStore;
                            tempStore.id = int.Parse(reader[0].ToString());
                            tempStore.name = reader[1].ToString();
                            tempStore.owner = reader[2].ToString();
                            tempStore.products = new List<Product>();

                            Product tempProduct;
                            tempProduct.id = int.Parse(reader[3].ToString());
                            tempProduct.name = reader[4].ToString();
                            tempProduct.price = int.Parse(reader[5].ToString());

                            tempStore.products.Add(tempProduct);
                            tempList.Add(tempStore);
                        }
                    }
                }

                con.Close();

                return tempList;
            }
        }*/

        public List<Employee> GetAllEmployeesWithStore()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                List<Employee> tempList = new List<Employee>();

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                string query = "SELECT employee_id, public.employee.name, surname, work_experience, work_store_id, public.store.name " +
                                    "FROM public.employee " +
                                        "INNER JOIN public.store ON public.employee.work_store_id = public.store.id_store ";

                Analyze(query);

                using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Employee tempSolo;

                        tempSolo.id = int.Parse(reader[0].ToString());
                        tempSolo.name = reader[1].ToString();
                        tempSolo.surname = reader[2].ToString();

                        tempSolo.work_experience = int.Parse(reader[3].ToString());
                        tempSolo.store_id = int.Parse(reader[4].ToString());                       

                        tempList.Add(tempSolo);
                    }
                }

                con.Close();
                return tempList;
            }
        }

        public void GetAllStores()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                string query = "SELECT * FROM public.store ORDER BY id_store ASC ";

                Analyze(query);

                using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                {
                    NpgsqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
 
                        Console.WriteLine(reader[1].ToString() + " with " + reader[2].ToString() + " tracks.");
                         
                    }
                }

                con.Close();
            }
        }

        public bool TestConnection()
        {
            using (NpgsqlConnection con = GetConnection())
            {
                con.Open();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                    return true;                   
                }
                else
                {
                    con.Close();
                    return true;
                }              
            }
        }

        public bool Connect(int storeId, int productId)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "INSERT INTO public.R1(id_store, id_product)" +
                                    "VALUES('" + storeId + "', " + productId + "); ";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Wrong Id.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool Disconnect(int storeId, int productId)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.R1 WHERE id_store="+ storeId + " AND id_product="+ productId;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Wrong Id.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        private NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(wholeParam);
        }

        public bool Recruit(int storeId, int number)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    for (int i = 0; i < number; i++)
                    {
                        string query = "INSERT INTO public.employee (name, surname, work_store_id) " +
                                    "VALUES((select chr(trunc(65+random()*25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "from generate_series(1, 1))"+

                                        ", "+

                                        "(select chr(trunc(65 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "|| chr(trunc(97 + random() * 25)::int)" +
                                        "from generate_series(1, 1)), "

                                    + storeId + "); ";

                        using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }
                return isOk;
            }
        }

        public bool RemoveEmployee(string name, string surname)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.employee WHERE name='"+name+"' AND surname='"+surname + "'";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveEmployee(int id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.employee WHERE employee_id=" +id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveProduct(string name)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.product WHERE name='" + name+"'";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveProduct(int id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.product WHERE id_product=" + id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool RemoveStore(string name)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.store WHERE name='" + name + "'";

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }

                return isOk;
            }
        }

        public bool Removestore(int id)
        {
            using (NpgsqlConnection con = GetConnection())
            {
                bool isOk = true;

                con.Open(); //opens the connection
                if (con.State == ConnectionState.Open)
                {
                     
                    Console.WriteLine("Conneccted to a DB.");
                     
                }
                else
                {
                     
                    Console.WriteLine("Connecction failed.");
                     
                    Environment.Exit(1);
                }

                try
                {
                    string query = "DELETE FROM public.store WHERE id_store=" + id;

                    using (NpgsqlCommand command = new NpgsqlCommand(query, con))
                    {
                        command.ExecuteNonQuery();
                    }
                }
                catch
                {
                     
                    Console.WriteLine("Error.");
                     
                    isOk = false;
                }

                return isOk;
            }
        }
    }
}
