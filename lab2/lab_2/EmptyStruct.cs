﻿using System;
using System.Collections.Generic;

namespace lab_2
{
    public struct Employee
    {
        public int id;
        public string name;
        public string surname;
            
        public int work_experience;
        public int store_id;
    }

    public struct Product
    {
        public int id;
        public string name;
        public int price;
    }

    public struct Store
    {
        public int id;
        public string name;
        public string owner;
        public List<Product> products;
    }
}
