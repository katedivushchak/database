﻿using System;
using System.Collections.Generic;

namespace lab_2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Interface.Run();
        }   
    }

    public static class Interface 
    {
        private static string input;
        private static string[] separated = new string[0];

        public static void Run()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8; // для виводу кирилиці
            Console.InputEncoding = System.Text.Encoding.UTF8; // для вводу кирилиці
            Console.Clear();

            Storage local_storage = new Storage("localhost", 5432, "ekaterinadivushchak", "kate28", "ekaterinadivushchak");   

            while (true)
            {         
                if (separated.Length > 0)
                {
                    Console.WriteLine("\nInput:");

                    foreach (string temp in separated)
                    {
                        Console.WriteLine("   "+temp);
                    }
                    Console.WriteLine();
                }

                MainMenuPrinter();

                input = Console.ReadLine();
                separated = input.Split('/');

                Console.Clear();

                if (separated.Length == 1)
                {
                    if (separated[0] == "exit")
                    {
                        Console.WriteLine("\nProgram is stoped.\n");
                         
                        break;
                    }
                    else if (separated[0] == "test")
                    {
                        Console.WriteLine("Testing connection...");

                        if (local_storage.TestConnection())
                        {
                            Console.WriteLine("Conneccted to a DB.");
                             
                        }
                        else
                        {
                             
                            Console.WriteLine("Connecction failed.");
                             
                        }
                    }
                }
                else if (separated.Length == 2)
                {
                    if (separated[0] == "get")
                    {
                        if (separated[1] == "employee")
                        {
                            // Вывести всех сотрудников
                            EntityPrinter.Employees(local_storage.GetAllEmployeesWithStore());
                        }
                      
                       /* else if (separated[1] == "store")
                        {
                            EntityPrinter.Stores(local_storage.GetAllStoresWithProducts());
                        }*/
                    }
                }
                else if (separated.Length == 3)
                {
                    if (separated[0] == "connect")
                    {
                        if (!string.IsNullOrEmpty(separated[1]) &&  
                                !string.IsNullOrEmpty(separated[2]))   
                        {
                            if (int.TryParse(separated[1], out _) && int.TryParse(separated[2], out _))
                            {
                                if (int.Parse(separated[1]) > 0 && int.Parse(separated[2]) > 0)
                                {
                                    local_storage.Connect(int.Parse(separated[1]), int.Parse(separated[2]));
                                }
                                else
                                {
                                     
                                    Console.WriteLine("Params must be more than 0.");
                                     
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("Params must be integer.");
                                 
                            }
                        }
                        else
                        {
                             
                            Console.WriteLine("Params must be not empty.");
                             
                        }
                    }
                    else if (separated[0] == "disconnect")
                    {
                        if (!string.IsNullOrEmpty(separated[1]) &&  
                                !string.IsNullOrEmpty(separated[2]))   
                        {
                            if (int.TryParse(separated[1], out _) && int.TryParse(separated[2], out _))
                            {
                                if (int.Parse(separated[1]) > 0 && int.Parse(separated[2]) > 0)
                                {
                                    local_storage.Disconnect(int.Parse(separated[1]), int.Parse(separated[2]));
                                }
                                else
                                {
                                     
                                    Console.WriteLine("Params must be more than 0.");
                                     
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("Params must be integer.");
                                 
                            }
                        }
                        else
                        {
                             
                            Console.WriteLine("Params must be not empty.");
                             
                        }
                    }
                    else if (separated[0] == "remove")
                    {
                        if (separated[1] == "employee")
                        {
                            if (!string.IsNullOrEmpty(separated[2]))    // id
                            {
                                if (int.TryParse(separated[2], out _))
                                {
                                    if (int.Parse(separated[2]) > 0)
                                    {
                                        local_storage.RemoveEmployee(int.Parse(separated[2]));
                                    }
                                    else
                                    {
                                         
                                        Console.WriteLine("ID param must be more than 1.");
                                         
                                    }
                                }
                                else
                                {
                                     
                                    Console.WriteLine("ID param must be integer.");
                                     
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("ID param must be not empty.");
                                 
                            }
                        }
                        
                            else
                            {
                                 
                                Console.WriteLine("Param must be not empty.");
                                 
                            }
                        }
                        else if (separated[1] == "product")
                        {
                            if (!string.IsNullOrEmpty(separated[2]))    // id OR name
                            {
                                if (int.TryParse(separated[2], out _))
                                {
                                    if (int.Parse(separated[2]) > 0)
                                    {
                                        local_storage.RemoveProduct(int.Parse(separated[2]));
                                    }
                                    else
                                    {
                                         
                                        Console.WriteLine("ID param must be more than 1.");
                                         
                                    }
                                }
                                else
                                {
                                    local_storage.RemoveProduct(separated[2]);
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("Param must be not empty.");
                                 
                            }
                        }
                        else if (separated[1] == "store")
                        {
                            if (!string.IsNullOrEmpty(separated[2]))    // id OR name
                            {
                                if (int.TryParse(separated[2], out _))
                                {
                                    if (int.Parse(separated[2]) > 0)
                                    {
                                        local_storage.RemoveStore(separated[2]);
                                    }
                                    else
                                    {
                                         
                                        Console.WriteLine("ID param must be more than 1.");
                                         
                                    }
                                }
                                else
                                {
                                    local_storage.RemoveStore(separated[2]);
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("Param must be not empty.");
                                 
                            }
                        }
                    }
                
                else if (separated.Length == 4)
                {
                    if (separated[0] == "put")
                    {
                        if (separated[1] == "store")
                        {
                            Console.WriteLine("Trying to input a data about new store...");

                            if (!string.IsNullOrEmpty(separated[2]) &&  
                                !string.IsNullOrEmpty(separated[3]))   
                            {
                                if (int.TryParse(separated[3], out _))
                                {
                                    if (int.Parse(separated[3]) > 0)
                                    {
                                        local_storage.InputStore(separated[2], separated[3]);
                                    }
                                    
                                }
                                
                            }
                            else
                            {
                                 
                                Console.WriteLine("Params must be not empty.");
                                 
                            }
                        }
                        

                    }
                    else if (separated[0] == "remove")
                    {
                        if (separated[1] == "employee")
                        {
                            if (!string.IsNullOrEmpty(separated[2]) &&  // name
                                !string.IsNullOrEmpty(separated[3]))    // surname
                            {
                                local_storage.RemoveEmployee(separated[2], separated[3]);   
                            }
                            else
                            {
                                 
                                Console.WriteLine("Params must be not empty.");
                                 
                            }
                        }
                    }
                    else if (separated[0] == "recruit")
                    {
                        if (!string.IsNullOrEmpty(separated[1]) &&    // station id
                             !string.IsNullOrEmpty(separated[2]))
                        {
                            if (int.TryParse(separated[1], out _) && int.TryParse(separated[2], out _))
                            {
                                if (int.Parse(separated[1]) > 0 && int.Parse(separated[2]) > 0)
                                {
                                    local_storage.Recruit(int.Parse(separated[1]), int.Parse(separated[2]));
                                }
                                else
                                {
                                     
                                    Console.WriteLine("Params must be more than 1.");
                                     
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("Params must be integer.");
                                 
                            }
                        }
                    }
                }
                else if (separated.Length == 5)
                {
                    if (separated[0] == "put")
                    {
                        if (separated[1] == "product")
                        {
                            Console.WriteLine("Trying to input a data about new product...");

                            if (!string.IsNullOrEmpty(separated[2]) &&  // name
                                !string.IsNullOrEmpty(separated[3]) )
                            {
                                if (int.TryParse(separated[3], out _))
                                {
                                    if (int.Parse(separated[3]) > 1 && int.Parse(separated[3]) > 1)
                                    {
                                        local_storage.InputProduct(separated[2], int.Parse(separated[3]));
                                    }
                                    else
                                    {
                                         
                                        Console.WriteLine("Price must be more than 0.");
                                         
                                    }
                                }
                                else
                                {
                                     
                                    Console.WriteLine("Price must be integer.");
                                     
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("Params must be not empty.");
                                 
                            }
                        }
                    }
                    else if (separated[0] == "update")
                    { 
                        if (separated[1] == "store")
                        {
                            Console.WriteLine("Trying to input a data about new store...");

                            if (!string.IsNullOrEmpty(separated[2]) &&  // id
                                !string.IsNullOrEmpty(separated[3]) &&  // name
                                !string.IsNullOrEmpty(separated[4]))   
                            {
                                if (int.TryParse(separated[2], out _) && int.TryParse(separated[4], out _))
                                {
                                    if (int.Parse(separated[2]) > 0 && int.Parse(separated[4]) > 0)
                                    {
                                        local_storage.UpdateStore(int.Parse(separated[2]), separated[3], separated[4]);
                                    }
                                    else
                                    {
                                         
                                        Console.WriteLine("ID must be more than 0.");
                                         
                                    }
                                }
                                else
                                {
                                     
                                    Console.WriteLine("ID must be integer.");
                                     
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("Params must be not empty.");
                                 
                            }
                        }
                    }
                }
                else if (separated.Length == 6)
                {
                    if (separated[0] == "put")
                    {
                        if (separated[1] == "employee")
                        {
                            Console.WriteLine("Trying to input a data about new employee...");

                            if (!string.IsNullOrEmpty(separated[2]) &&   // name
                                !string.IsNullOrEmpty(separated[3]) &&   // surname
                                !string.IsNullOrEmpty(separated[4]) &&  
                                !string.IsNullOrEmpty(separated[5]))    
                            {
                                if (int.TryParse(separated[4], out _) && int.TryParse(separated[5], out _))
                                {
                                    local_storage.InputEmployee(separated[2], separated[3], int.Parse(separated[4]), int.Parse(separated[4]));
                                }
                                else
                                {
                                     
                                    Console.WriteLine("IDs must be integer.");
                                     
                                }
                            }
                        }
                        else
                        {
                             
                            Console.WriteLine("All params must be not empty.");
                             
                        }
                    }
                    else if (separated[0] == "update")
                    {
                        if (separated[1] == "product")
                        {
                            Console.WriteLine("Trying to update a data about the product...");

                            if (!string.IsNullOrEmpty(separated[2]) &&  // id
                                !string.IsNullOrEmpty(separated[3]) &&  // name
                                !string.IsNullOrEmpty(separated[4]))
                            {
                                if (int.TryParse(separated[2], out _) && int.TryParse(separated[4], out _))
                                {
                                    if (int.Parse(separated[2]) > 1 && int.Parse(separated[4]) > 1)
                                    {
                                        local_storage.UpdateProduct(int.Parse(separated[2]), separated[3], int.Parse(separated[4]));
                                    }
                                    else
                                    {
                                         
                                        Console.WriteLine("Id and price must be more than 1.");
                                         
                                    }
                                }
                                else
                                {
                                     
                                    Console.WriteLine("Id and price must be integer.");
                                     
                                }
                            }
                            else
                            {
                                 
                                Console.WriteLine("Params must be not empty.");
                                 
                            }
                        }
                    }
                }
                else if (separated.Length == 7)
                {
                    if (separated[0] == "update")
                    {
                        if (separated[1] == "employee")
                        {
                            Console.WriteLine("Trying to update a data about the employee...");

                            if (!string.IsNullOrEmpty(separated[2]) &&   // id
                                !string.IsNullOrEmpty(separated[3]) &&   // name
                                !string.IsNullOrEmpty(separated[4]) &&   // surname
                                !string.IsNullOrEmpty(separated[5]) &&   
                                !string.IsNullOrEmpty(separated[6]))    
                            {
                                if (int.TryParse(separated[2], out _) && int.TryParse(separated[5], out _) && int.TryParse(separated[6], out _))
                                {
                                    local_storage.UpdateEmployee(int.Parse(separated[2]), separated[3], separated[4], int.Parse(separated[5]), int.Parse(separated[6]));
                                }
                                else
                                {
                                     
                                    Console.WriteLine("IDs must be integer.");
                                     
                                }
                            }
                        }
                        else
                        {
                             
                            Console.WriteLine("All params must be not empty.");
                             
                        }
                    }
                }
            }
        }

        public static void MainMenuPrinter()
        {
            Console.WriteLine("Commands:");
             
            Console.WriteLine("test - test connection");
            Console.WriteLine("exit - exit the program");

            Console.WriteLine("   Getting:");
             
            Console.WriteLine("       get/employee - all employees with store name");
            Console.WriteLine("       get/store - store with products");

            Console.WriteLine("   Adding:");
             
            Console.WriteLine("       - put/employee/[name]/[surname]/[work_experience]/[work_store_id] - add an employee");
            Console.WriteLine("       - put/product/[name]/[price] - add a product");
            Console.WriteLine("       - put/store/[name]/[owner] - add a store");

            Console.WriteLine("   Updating:");
             
            Console.WriteLine("       - update/employee/[id]/[name]/[surname]/[work_experience]/[work_store_id] - update an employee");
            Console.WriteLine("       - update/product/[id]/[name]/[price] - update a product");
            Console.WriteLine("       - update/store/[id]/[name]/[owner] - update a store");

            Console.WriteLine("   Removing:");
             
            Console.WriteLine("       - remove/employee/[name]/[surname] - remove an employee");
            Console.WriteLine("       - remove/employee/[id] - remove an employee by id");

            Console.WriteLine("       - remove/product/[name] - remove a product (with connections to stores)");
            Console.WriteLine("       - remove/product/[id] - remove a product (with connections to stores)");

            Console.WriteLine("       - remove/store/[name] - remove a store (but not the product)");
            Console.WriteLine("       - remove/store/[id] - remove a store (but not the product)");

            Console.WriteLine("");
        }
    }

    public static class EntityPrinter
    {
        public static void Employees(List<Employee> tempList)
        {
            Console.WriteLine("\nAll store employees:");
             

            foreach (Employee solo in tempList)
            {
                Console.WriteLine("ID: "+solo.id);
                Console.Write("Name: ");
                Console.WriteLine(solo.name);
                Console.Write("Surname: ");
                Console.WriteLine(solo.surname);
                 
                Console.WriteLine("Work Experience: " + solo.work_experience);
                Console.WriteLine("Store: " + solo.store_id);
            }
        }


        public static void Stores(List<Store> tempList)
        {
            Console.WriteLine();
            foreach(Store soloStore in tempList)
            {
                Console.WriteLine("Store "+ soloStore.name+", owner: "+ soloStore.owner+" (ID "+ soloStore.id+")");
                 

                foreach (Product soloProduct in soloStore.products)
                {
                    Console.WriteLine("   ~ "+ soloProduct.name+" (ID "+ soloProduct.id+", price: "+ soloProduct.price+" $)");
                }
            }
        }
    }
}


